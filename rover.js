const prompt = require("prompt-sync")();

let plateauSize = {
  x: 0,
  y: 0,
};
const roverStatus = {
  position: {
    x: 0,
    y: 0,
  },
  velocity: {
    x: 0,
    y: 0,
  },
  direction: "N",
};

function main() {
  if (!promptPlateau()) return;
  if (!promptLanding()) return;
  promptInstructions();
}

function promptPlateau() {
  let args = prompt("Plateau: ");
  const argsPlateau = args.split(" ");
  if (argsPlateau.length !== 2) {
    console.error("ERROR! Require 2 args <X> <Y>.");
    return false;
  }
  let [parsedX, errX] = validateInt(argsPlateau[0]);
  let [parsedY, errY] = validateInt(argsPlateau[1]);
  if (errX || errY) return false;
  setPlateauSize(parsedX, parsedY);

  return true;
}

function promptLanding() {
  args = prompt("Rover1 Landing: ");
  const argsLanding = args.split(" ");
  if (argsLanding.length !== 3) {
    console.error("ERROR! Require 3 args <X> <Y> <Direction>.");
    return false;
  }
  let [parsedX, errX] = validateInt(argsLanding[0]);
  let [parsedY, errY] = validateInt(argsLanding[1]);
  if (errX || errY) return false;
  if (!validateDirection(argsLanding[2])) return false;

  setLanding(parseInt(parsedX), parseInt(parsedY), argsLanding[2]);
  return true;
}

function promptInstructions() {
  args = prompt("Rover1 Instructions: ");
  processInstructions(args);
  console.log(
    `Rover 1: ${roverStatus.position.x} ${roverStatus.position.y} ${roverStatus.direction}`
  );
}

function setPlateauSize(x, y) {
  plateauSize = { x, y };
}

function setLanding(x, y, direction) {
  if (validatePosition(x, y)) {
    roverStatus.position.x = x;
    roverStatus.position.y = y;
  }
  if (validateDirection(direction)) {
    setRoverDirection(direction);
  }
}

function processInstructions(inputString) {
  for (let i = 0; i < inputString.length; i++) {
    processInputChar(inputString.charAt(i));
  }
}

function processInputChar(inputChar) {
  switch (inputChar) {
    case "R":
      rotateRover(true);
      break;
    case "L":
      rotateRover(false);
      break;
    case "M":
      moveRover();
      break;
    default:
      console.error("ERROR! Invalid input: " + inputChar);
  }
}

function rotateRover(rotateClockwise) {
  if (rotateClockwise) {
    switch (roverStatus.direction) {
      case "N":
        setRoverDirection("E");
        break;
      case "W":
        setRoverDirection("N");
        break;
      case "S":
        setRoverDirection("W");
        break;
      case "E":
        setRoverDirection("S");
        break;
    }
  } else {
    switch (roverStatus.direction) {
      case "N":
        setRoverDirection("W");
        break;
      case "W":
        setRoverDirection("S");
        break;
      case "S":
        setRoverDirection("E");
        break;
      case "E":
        setRoverDirection("N");
        break;
    }
  }
}

function setRoverDirection(direction) {
  roverStatus.direction = direction;

  switch (direction) {
    case "N":
      setRoverVelocity(0, 1);
      break;
    case "W":
      setRoverVelocity(-1, 0);
      break;
    case "S":
      setRoverVelocity(0, -1);
      break;
    case "E":
      setRoverVelocity(1, 0);
      break;
    default:
      console.error("ERROR! Invalid direction: " + direction);
  }
}

function setRoverVelocity(x, y) {
  roverStatus.velocity.x = x;
  roverStatus.velocity.y = y;
}

function moveRover() {
  roverStatus.position.x = Math.max(
    0,
    Math.min(plateauSize.x, roverStatus.position.x + roverStatus.velocity.x)
  );
  roverStatus.position.y = Math.max(
    0,
    Math.min(plateauSize.y, roverStatus.position.y + roverStatus.velocity.y)
  );
}

function validateInt(x) {
  let parsedX = parseInt(x);
  if (isNaN(parsedX)) {
    console.error("ERROR! Invalid X or Y value: NaN.");
    return [parsedX, 1];
  }
  return [parsedX, 0];
}

function validatePosition(x, y) {
  if (x < 0 || x > plateauSize.x) {
    console.error("ERROR! X position exceeds plateau size!");
    return false;
  }
  if (y < 0 || y > plateauSize.y) {
    console.error("ERROR! Y position exceeds plateau size!");
    return false;
  }
  return true;
}

function validateDirection(direction) {
  const validDirections = ["N", "W", "S", "E"];
  if (validDirections.includes(direction)) return true;

  console.error("ERROR! Invalid direction: " + direction);
  return false;
}

function getRoverStatus() {
  return roverStatus;
}

let functions = {
  main,
  setPlateauSize,
  setLanding,
  processInstructions,
  getRoverStatus,
};

module.exports = functions;
