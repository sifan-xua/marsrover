const assert = require("assert");
const rover = require("../rover.js");

describe("Rover Tests", function () {
  describe("Unit Tests for Critical Functions on Plateau Size 5 5", function () {
    rover.setPlateauSize(5, 5);

    it("Testing Edge Case: 0 0 N -> move left should return position 0 0 W", function () {
      rover.setLanding(0, 0, "N");
      rover.processInstructions("LM");

      const roverStatus = rover.getRoverStatus();
      assert(roverStatus.position, { x: 0, y: 0 });
      assert(roverStatus.direction, "W");
    });

    it("Testing Edge Case: 0 0 N -> move down should return position 0 0 S", function () {
      rover.setLanding(0, 0, "N");
      rover.processInstructions("LLM");

      const roverStatus = rover.getRoverStatus();
      assert(roverStatus.position, { x: 0, y: 0 });
      assert(roverStatus.direction, "S");
    });

    it("Testing Edge Case: 5 5 N -> move right should return position 5 5 E", function () {
      rover.setLanding(5, 5, "N");
      rover.processInstructions("RM");

      const roverStatus = rover.getRoverStatus();
      assert(roverStatus.position, { x: 5, y: 5 });
      assert(roverStatus.direction, "E");
    });

    it("Testing Edge Case: 5 5 N -> move up should return position 5 5 N", function () {
      rover.setLanding(5, 5, "N");
      rover.processInstructions("M");

      const roverStatus = rover.getRoverStatus();
      assert(roverStatus.position, { x: 5, y: 5 });
      assert(roverStatus.direction, "N");
    });
  });

  describe("System Test for Plateau Size: 5 5", function () {
    rover.setPlateauSize(5, 5);

    it("(Landing 1 2 N, instructions LMLMLMLMM): should return position 1 3 N", function () {
      rover.setLanding(1, 2, "N");
      rover.processInstructions("LMLMLMLMM");

      const roverStatus = rover.getRoverStatus();
      assert.deepStrictEqual(roverStatus.position, { x: 1, y: 3 });
      assert(roverStatus.direction, "N");
    });

    it("(Landing 3 3 E, instructions MMRMMRMRRM): should return position 5 1 E", function () {
      rover.setLanding(3, 3, "E");
      rover.processInstructions("MMRMMRMRRM");

      const roverStatus = rover.getRoverStatus();
      assert.deepStrictEqual(roverStatus.position, { x: 5, y: 1 });
      assert(roverStatus.direction, "E");
    });
  });
});
